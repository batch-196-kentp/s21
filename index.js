console.log("Hello World");

originalArray = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];
console.log("Original Array:");
console.log(originalArray);


function addInputToArray(input){
	originalArray.push(input);
	console.log(originalArray);
}

addInputToArray('John Cena');


function receiveIndexNumber(indexNum){
	return originalArray[indexNum];
}

let itemFound = receiveIndexNumber(2);
console.log(itemFound);

function deleteObject(deleteIndex){
	return originalArray[deleteIndex-1]
}

let dlete = deleteObject(5)
console.log(dlete);

function updateArray(index,update) {
	originalArray[index] = update;
	console.log(originalArray);
}

updateArray(2, "Triple H");

function checkArray() {
    if(originalArray.length > 0) {
        return false;
    } else {
        return true;
    }
}

let isUsersEmpty = checkArray();
console.log(isUsersEmpty);